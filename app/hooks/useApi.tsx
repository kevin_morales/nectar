import { ApiResponse } from "apisauce";
import { useState } from "react";

type NectarApiResponse<T> = {
  request: (...args: any) => Promise<ApiResponse<any, any>>;
  data: any;
  error: boolean;
  loading: boolean;
};

const useApi = <T,>(
  apiFunc: (...args: any) => Promise<ApiResponse<any>>
): NectarApiResponse<any> => {
  const [data, setData] = useState<T | null>(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const request = async (...args: any): Promise<ApiResponse<any, any>> => {
    setLoading(true);
    const response = await apiFunc(...args);
    setLoading(false);
    console.log(response)
    setError(response.ok == false);
    setData(response.data);
    return response;
  };

  return { request, data, error, loading };
};

export default useApi;
