import { NativeSyntheticEvent, TextInputChangeEventData } from "react-native";

export const removeEmptySpaces = (stringVal: string): boolean => {
  return /\s/g.test(stringVal);
};

export const stripWhiteSpace = (
  e: NativeSyntheticEvent<TextInputChangeEventData>
): void => {
  const isValid = removeEmptySpaces(e.nativeEvent.text);
};
