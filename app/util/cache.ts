import AsyncStorage from "@react-native-async-storage/async-storage";
import dayjs from "dayjs";
import { logger } from "react-native-logs";

const log = logger.createLogger();

interface CacheItem<T> {
  value: T;
  timestamp: string;
}

const prefix = "cache";

const isExpired = (timestamp: string): boolean => {
  const now = dayjs();
  const storedTime = dayjs(timestamp);
  const expiryTime = storedTime.add(5, "minutes");
  const expired = now.isAfter(expiryTime);
  return expired;
};

const store = async <T>(key: string, value: any): Promise<void> => {
  try {
    const now = dayjs().toISOString();
    const item: CacheItem<T> = {
      value,
      timestamp: now,
    };
    await AsyncStorage.setItem(prefix + key, JSON.stringify(item));
  } catch (error) {
    log.error(error);
  }
};

const get = async <T>(key: string): Promise<CacheItem<T> | null> => {
  try {
    const value = await AsyncStorage.getItem(prefix + key);
    if (!value) return null;

    const item: CacheItem<T> = JSON.parse(value);
    if (!item) return null;

    if (isExpired(item.timestamp)) {
      await AsyncStorage.removeItem(prefix + key);
      return null;
    }
    return item;
  } catch (error) {
    log.error(error);
    return null;
  }
};

const cache = {
  get,
  store,
};

export default cache;
