import jwtDecode from "jwt-decode";
import { useContext } from "react";
import { User } from "~/config/types";
import AuthContext from "./context";
import authStorage from "./storage";

const useAuth = () => {
  const { user, setUser } = useContext(AuthContext);

  const login = (authToken: string): void => {
    const user = jwtDecode<User>(authToken);
    setUser(user);
    authStorage.storeToken(authToken);
  };

  const logOut = (): void => {
    setUser(undefined);
    authStorage.removeToken();
  };

  return { user, login, logOut };
};

export default useAuth;
