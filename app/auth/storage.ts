import * as SecureStore from "expo-secure-store";
import jwtDecode from "jwt-decode";
import { logger } from "react-native-logs";
import { User } from "~/config/types";

const log = logger.createLogger();
const key = "authToken";

const storeToken = async (authToken: string): Promise<void> => {
  try {
    await SecureStore.setItemAsync(key, authToken);
  } catch (error) {
    log.error("Error storing auth token", error);
  }
};

const getToken = async (): Promise<string | null> => {
  try {
    const token = await SecureStore.getItemAsync(key);
    return token;
  } catch (error) {
    log.error("Error getting auth token", error);
    return null;
  }
};

const removeToken = async (): Promise<void> => {
  try {
    await SecureStore.deleteItemAsync(key);
  } catch (error) {
    log.error("Error removing the auth token", error);
  }
};

const getUser = async (): Promise<User | null> => {
  const token = await getToken();
  if (token) {
    const user = jwtDecode<User>(token);
    return user;
  }
  return null;
};

const authStorage = {
  getToken,
  getUser,
  removeToken,
  storeToken,
};

export default authStorage;
