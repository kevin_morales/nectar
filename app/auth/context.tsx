import { createContext } from "react";
import { User } from "../config/types";

export interface AuthContextProps {
  user?: User;
  setUser: (user?: User) => void;
}

const initialValues = {
  user: undefined,
  setUser: (_?: User) => {},
};

const AuthContext = createContext<AuthContextProps>(initialValues);

export default AuthContext;
