import { ImageSourcePropType } from "react-native";

export type IFormPickerItem = number | string | any;

export interface Plant {
  id?: string;
  name: string;
  userId?: string;
  images: any[];
  category?: PlantCategory | null;
  categoryId: number;
  createdAt?: string;
}

export interface PlantCategory {
  backgroundColor: string;
  icon: string;
  label: string;
  value: number;
}

export interface LoginProps {
  email: string;
  password: string;
}

export interface PickerItemData {
  label: string;
}

export interface CategoryPickerItemData extends PickerItemData {
  backgroundColor: string;
  icon: string;
}

export interface User {
  email: string;
  iat?: number;
  name: string;
  userId: string;
  id: string;
}
