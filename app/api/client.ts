import { ApiOkResponse, ApiResponse, create } from "apisauce";
import { AxiosRequestConfig } from "axios";
import authStorage from "../auth/storage";
import cache from "../util/cache";

const client = create({
  //Go api
  baseURL: "http://192.168.1.200:8080/api/",
});

client.addAsyncRequestTransform(async (request) => {
  const authToken = await authStorage.getToken();
  if (!authToken) return;
  request.headers["Authorization"] = `Bearer ${authToken}`;
});

const get = client.get;
client.get = async <T, U = T>(
  url: string,
  params?: {},
  axiosConfig?: AxiosRequestConfig
): Promise<ApiResponse<T, U>> => {
  const response = await get<any>(url, params, axiosConfig);
  if (response.ok) {
    cache.store<T>(url, response.data);
    return response;
  }
  const cachedData = await cache.get<T>(url);
  if (cachedData) {
    const okResponse: ApiOkResponse<T> = {
      ok: true,
      data: cachedData.value,
      problem: null,
      originalError: null,
    };
    return okResponse;
  }
  return response;
};

export default client;
