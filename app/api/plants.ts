import { ApiResponse, create } from "apisauce";
import { Plant } from "../config/types";
import client from "./client";

const getEndpoint = "v1/plant/user/";

const getPlants = (userId: string): Promise<ApiResponse<Plant[]>> => {
  return client.get<Plant[]>(
    `${getEndpoint}${userId}`
  );
};

const addPlant = (
  plant: Plant,
  onUploadProgress: (progress: number) => void
): Promise<ApiResponse<any>> => {
  const formData = new FormData();

  plant.images.forEach((image, index) =>
    formData.append("image" + index, {
      name: image,
      type: "image/jpeg",
      uri: image,
    })
  )

  const urlWithParams = `v1/plant?userId=${encodeURIComponent(plant.userId!)}&plantName=${encodeURIComponent(plant.name)}&catId=${encodeURIComponent(plant.categoryId)}&numImages=${plant.images.length}`

  console.log(urlWithParams)

  return client.post<any>(urlWithParams, formData, {
    onUploadProgress: (progress) =>
      onUploadProgress(progress.loaded / progress.total),
  });
};

const plantsApi = {
  getPlants,
  addPlant,
};
export default plantsApi;
