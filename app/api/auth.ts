import { ApiResponse, create } from "apisauce";
import client from "./client";

const login = (email: string, password: string): Promise<ApiResponse<any>> => {
  return client.post<any>("/v1/auth", { email, password });
};

const authApi = {
  login,
};

export default authApi;
