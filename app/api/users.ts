import { ApiResponse } from "apisauce";
import client from "./client";

const register = (userInfo: any): Promise<ApiResponse<any>> => {
  return client.post<any>("/v1/user", userInfo);
};

const getUserInfo = (id: string): Promise<ApiResponse<any>> => {
  console.log("id in getUserInfo: " + id)
  return client.get<any>("v1/user/" + id);
};

const usersApi = {
  register,
  getUserInfo,
};

export default usersApi;
