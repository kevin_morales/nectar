import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { StackNavigationProp } from "@react-navigation/stack";

export type AuthStackParamList = {
  Welcome: undefined;
  Login: any;
  Register: any;
};

export type WelcomeScreenProps = NativeStackScreenProps<
  AuthStackParamList,
  "Welcome"
>;
export type LoginScreenProps = NativeStackScreenProps<
  AuthStackParamList,
  "Login"
>;
export type RegisterScreenProps = NativeStackScreenProps<
  AuthStackParamList,
  "Register"
>;

export type AuthNavigationProps = StackNavigationProp<AuthStackParamList>;
