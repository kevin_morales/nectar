import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { StackNavigationProp } from "@react-navigation/stack";

export type AccountStackParamList = {
  Account: any;
  Messages: any;
};

export type AccountScreenProps = NativeStackScreenProps<
  AccountStackParamList,
  "Account"
>;
export type MessagesScreenProps = NativeStackScreenProps<
  AccountStackParamList,
  "Messages"
>;

export type AccountNavigationProps = StackNavigationProp<AccountStackParamList>;
