import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { StackNavigationProp } from "@react-navigation/stack";
import { Plant } from "../../config/types";

export type FeedStackParamList = {
  PlantListings: any;
  PlantDetails: Plant;
};

export type PlantListScreenProps = NativeStackScreenProps<
  FeedStackParamList,
  "PlantListings"
>;
export type PlantDetailsScreenProps = NativeStackScreenProps<
  FeedStackParamList,
  "PlantDetails"
>;

export type FeedNavigationProps = StackNavigationProp<FeedStackParamList>;
