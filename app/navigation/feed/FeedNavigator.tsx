import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import PlantListScreen from "../../screens/PlantListScreen";
import PlantDetailsScreen from "../../screens/PlantDetailsScreen";

const Stack = createStackNavigator();

const FeedNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        presentation: "modal",
        gestureEnabled: true,
        gestureResponseDistance: 500,
        ...TransitionPresets.ModalPresentationIOS,
      }}
    >
      <Stack.Screen name="PlantListings" component={PlantListScreen} />

      <Stack.Screen name="PlantDetails" component={PlantDetailsScreen} />
    </Stack.Navigator>
  );
};

export default FeedNavigator;
