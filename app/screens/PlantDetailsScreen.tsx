import { Dimensions, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, View } from "react-native";
import { Image } from "react-native-expo-image-cache";
import NectarText from "../components/NectarText";
import colors from "../config/colors";
import { PlantDetailsScreenProps } from "../navigation/feed/types";

const formatDate = (dateUtcString?: string): string | null => {
  const date = dateUtcString ? new Date(dateUtcString) : null;
  if(date) return date.toLocaleString();
  return null;
}

const { width, height } = Dimensions.get("window");

const PlantDetailsScreen = ({ route }: PlantDetailsScreenProps) => {
  const plant = route.params;
  // Note: Server currently returns an array so we must use plant.images[0].url
  // to get the first images object in the array

  //format the created at date
  const createdAt = formatDate(plant.createdAt);

  return (
    <KeyboardAvoidingView
      behavior="position"
      keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
    >
      <ScrollView
        horizontal
        snapToInterval={width}
        decelerationRate="fast"
        pagingEnabled
        showsHorizontalScrollIndicator
      >
        {plant.images.map((image, index) =>(
          <Image
            style={styles.image}
            tint="light"
            preview={{ uri: image.thumbnailUrl }}
            uri={image.url}
            key={index}
          />
       ))}
      </ScrollView>
      <View style={styles.detailsContainer}>
        <NectarText style={styles.title}>{plant.name}</NectarText>
        {createdAt &&
          <NectarText style={styles.date}>
            {`Added on ${createdAt}`}
          </NectarText>
        }
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: 300,
    height: 300,
  },
  date: {
    color: colors.secondary,
    fontWeight: "bold",
    fontSize: 15,
    marginVertical: 10,
  },
  imageContainer: {
    width: width, 
    flex: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: "500",
  },
  userContainer: {
    marginVertical: 40,
  },
});

export default PlantDetailsScreen;
