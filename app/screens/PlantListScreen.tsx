import React, { useContext, useEffect, useState } from "react";
import { FlatList, StyleSheet } from "react-native";
import Card from "../components/Card";
import SafeScreen from "../components/SafeScreen";
import NectarText from "../components/NectarText";
import NectarButton from "../components/NectarButton";
import NectarActivityIndicator from "../components/NectarActivityIndicator";
import colors from "../config/colors";
import plantsApi from "../api/plants";
import useApi from "../hooks/useApi";
import { PlantListScreenProps } from "../navigation/feed/types";
import { Plant } from "~/config/types";
import AuthContext from "../auth/context";

const PlantListScreen = ({ navigation }: PlantListScreenProps) => {
  const { user } = useContext(AuthContext);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const getPlantsApi = useApi(plantsApi.getPlants);

  useEffect(() => {
    getPlantsApi.request(user?.id);
    return () => setIsRefreshing(false);
  }, []);

  const onRefresh = () => {
    //set isRefreshing to true
    setIsRefreshing(true);
    getPlantsApi.request(user?.id);
    setIsRefreshing(false);
  };

  return (
    <>
      <NectarActivityIndicator visible={getPlantsApi.loading} />
      <SafeScreen style={styles.screen}>
        {(getPlantsApi.error === true) ? (
          <>
            <NectarText>Could not retrieve plants</NectarText>
            <NectarButton title="Retry" onPress={getPlantsApi.request} />
          </>
        ) : (
        <FlatList
          data={getPlantsApi.data}
          numColumns={2}
          keyExtractor={(plant: Plant) => plant.id!}
          onRefresh={onRefresh}
          refreshing={isRefreshing}
          renderItem={({ item, index }) => (
            <Card
              imageUrl={item.images[0].url}
              onPress={() => navigation.navigate("PlantDetails", item)}
              style={{ marginHorizontal: 5 }}
              subTitle={`id: ${item.id!.toString()}`}
              thumbnailUrl={item.images[0].thumbnailUrl}
              title={item.name}
              date={item.createdAt!}
            />
          )}
          showsVerticalScrollIndicator={false}
        />
        )}
      </SafeScreen>
    </>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 5,
    backgroundColor: colors.light,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default PlantListScreen;
