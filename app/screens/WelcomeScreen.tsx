import React from "react";
import { Image, ImageBackground, StyleSheet, View } from "react-native";
import NectarButton from "../components/NectarButton";
import { WelcomeScreenProps } from "../navigation/auth/types";

const WelcomeScreen = ({ navigation }: WelcomeScreenProps) => {
  return (
      // @ts-ignore
    <ImageBackground
      style={styles.background}
      source={require("../assets/coolsplash.jpg")}
    >
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require("../assets/logo2.png")} />
      </View>
      <View style={styles.buttonContainer}>
        <NectarButton
          title={"Login"}
          onPress={() => navigation.navigate("Login")}
        />
        <NectarButton
          title={"Register"}
          onPress={() => navigation.navigate("Register")}
          color={"secondary"}
        />
      </View>
    </ImageBackground>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  buttonContainer: {
    padding: 20,
    width: "100%",
  },
  logo: {
    width: 100,
    height: 100,
  },
  logoContainer: {
    position: "absolute",
    top: 70,
    alignItems: "center",
  },
  tagline: {
    fontSize: 25,
    fontWeight: "600",
    paddingVertical: 20,
  },
});
