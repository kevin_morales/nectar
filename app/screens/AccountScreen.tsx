import { useEffect } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import Icon from "../components/Icon";
import ListItem from "../components/lists/ListItem";
import ListItemSeparator from "../components/lists/ListItemSeparator";
import NectarActivityIndicator from "../components/NectarActivityIndicator";
import NectarButton from "../components/NectarButton";
import NectarText from "../components/NectarText";
import SafeScreen from "../components/SafeScreen";
import colors from "../config/colors";
import {
  AccountScreenProps,
  AccountStackParamList,
} from "../navigation/account/types";
import usersApi from "../api/users";

interface MenuItem {
  title: string;
  icon: any;
  targetScreen: keyof AccountStackParamList;
}

const menuItems: MenuItem[] = [
  {
    title: "My Plants",
    icon: {
      name: "format-list-bulleted",
      backgroundColor: colors.primary,
    },
    targetScreen: "Account",
  },
];

const AccountScreen = ({ navigation }: AccountScreenProps) => {
  const { user, logOut } = useAuth();
  const getUserInfoApi = useApi(usersApi.getUserInfo);

  useEffect(() => {
    getUserInfoApi.request(user?.id);
  }, []);

  return (
    <>
      <NectarActivityIndicator visible={getUserInfoApi.loading} />
      <SafeScreen style={styles.screen}>
        {getUserInfoApi.error ||
        getUserInfoApi.data === null ||
        getUserInfoApi.data === undefined ? (
          <>
            <NectarText>Could not retrieve account information</NectarText>
            <NectarButton title="Retry" onPress={getUserInfoApi.request} />
          </>
        ) : (
          <>
            <View style={styles.container}>
              <ListItem
                title={`${getUserInfoApi.data.firstName} ${getUserInfoApi.data.lastName}`}
                description={getUserInfoApi.data.email}
                image={require("../assets/me.jpg")}
              />
            </View>
            <View style={styles.container}>
              <FlatList
                data={menuItems}
                keyExtractor={(menuItem) => menuItem.title}
                renderItem={({ item }) => (
                  <ListItem
                    title={item.title}
                    onPress={() => navigation.navigate(item.targetScreen)}
                    IconComponent={
                      <Icon
                        name={item.icon.name}
                        backgroundColor={item.icon.backgroundColor}
                      />
                    }
                  />
                )}
                ItemSeparatorComponent={ListItemSeparator}
              />
            </View>
          </>
        )}
        <ListItem
          title="Log Out"
          IconComponent={<Icon name="logout" backgroundColor="#2b00ff" />}
          onPress={logOut}
        />
      </SafeScreen>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  screen: {
    backgroundColor: colors.light,
  },
});

export default AccountScreen;
