import { useState } from "react";
import { StyleSheet, Image } from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";
import { logger } from "react-native-logs";
import { LoginProps } from "../config/types";
import { stripWhiteSpace } from "../util";
import authApi from "../api/auth";
import SafeScreen from "../components/SafeScreen";
import { ErrorMessage, FormField } from "../components/forms";
import NectarButton from "../components/NectarButton";
import useAuth from "../auth/useAuth";

const log = logger.createLogger();

const initialValues: LoginProps = {
  email: "",
  password: "",
};

const loginSchema = Yup.object({
  email: Yup.string()
    .trim("The email cannot include leading and trailing spaces")
    .required()
    .email()
    .label("Email"),
  password: Yup.string()
    .trim("The password name cannot include leading and trailing spaces")
    .required()
    .min(6, "Password must be at least 6 characters")
    .label("Password"),
});

const LoginScreen = () => {
  const { login } = useAuth();
  const [loginFailed, setLoginFailed] = useState(false);

  const handleSubmit = async (email: string, password: string) => {
    try {
      const result = await authApi.login(email, password);
      console.log(result);
      if (result.ok === false) {
        setLoginFailed(true);
        return;
      }
      setLoginFailed(false);
      const data = result.data;
      login(data.token);
    } catch (err: any) {
      log.error(err);
    }
  };

  return (
    <SafeScreen style={styles.container}>
      <Image style={styles.logo} source={require("../assets/logo2.png")} />
      <ErrorMessage
        visible={loginFailed}
        error={"Invalid email and/ or password"}
      />
      <Formik
        initialValues={initialValues}
        onSubmit={({ email, password }) => handleSubmit(email, password)}
        validationSchema={loginSchema}
      >
        {({ handleSubmit }) => (
          <>
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon="email"
              keyboardType="email-address"
              name="email"
              onChange={(e) => stripWhiteSpace(e)}
              placeholder="Email"
              textContentType="emailAddress"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon="lock"
              name="password"
              placeholder="Password"
              secureTextEntry
              textContentType="password"
            />
            <NectarButton title="Login" onPress={handleSubmit as any} />
          </>
        )}
      </Formik>
    </SafeScreen>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginTop: 50,
    marginBottom: 20,
  },
});

export default LoginScreen;
