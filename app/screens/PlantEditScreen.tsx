import { useContext, useState } from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";
import { Formik, FormikBag, FormikState } from "formik";
import { ErrorMessage, FormField, FormPicker } from "../components/forms";
import CategoryPickerItem from "../components/CategoryPickerItem";
import SafeScreen from "../components/SafeScreen";
import { Plant, PlantCategory } from "../config/types";
import FormImagePicker from "../components/forms/FormImagePicker";
import NectarButton from "../components/NectarButton";
import UploadScreen from "./UploadScreen";
import newPlantsApi from "../api/plants";
import AuthContext from "../auth/context";

type FormikResetFunc = (
  nextState?: Partial<FormikState<Plant>> | undefined
) => void;

const validationSchema = Yup.object().shape({
  name: Yup.string().required().min(1).label("Plant Name"),
  categories: Yup.array().of(
    Yup.object().required().nullable().label("Category")
  ),
  images: Yup.array().min(1, "Please select at least one image"),
});

const getPlantCategories = (): PlantCategory[] => {
  return [
    {
      backgroundColor: "#fc5c65",
      icon: "skull-crossbones",
      label: "Toxic to Pets",
      value: 1,
    },
    {
      backgroundColor: "#fd9644",
      icon: "leaf-maple",
      label: "Outdoor",
      value: 2,
    },
    {
      backgroundColor: "#fed330",
      icon: "home",
      label: "Indoor",
      value: 3,
    },
    {
      backgroundColor: "#26de81",
      icon: "white-balance-sunny",
      label: "Warm Weather",
      value: 4,
    },
    {
      backgroundColor: "#2bcbba",
      icon: "new-box",
      label: "New",
      value: 5,
    },
  ];
};

const plantCategories = getPlantCategories();

const initialValues: Plant = {
  name: "",
  category: null,
  images: [],
  categoryId: 0,
};

const PlantEditScreen = () => {
  const { user } = useContext(AuthContext);
  const [uploadVisible, setUploadVisible] = useState(false);
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState(false);
  const handleSubmit = async (plant: Plant, resetForm: FormikResetFunc) => {
    setProgress(0);
    setUploadVisible(true);
    plant.categoryId = plant.category!.value;
    plant.userId = user?.id;
    const result = await newPlantsApi.addPlant(plant, (progress: number) =>
      setProgress(progress)
    );
    if (!result.ok) {
      setUploadVisible(false);
      setError(true);
    }

    resetForm();
  };

  return (
    <SafeScreen style={styles.container}>
      <UploadScreen
        onDone={() => setUploadVisible(false)}
        progress={progress}
        visible={uploadVisible}
      />
      <Formik
        initialValues={initialValues}
        onSubmit={(values, { resetForm }) => handleSubmit(values, resetForm)}
        validationSchema={validationSchema}
      >
        {({ handleSubmit }) => (
          <>
            <ErrorMessage visible={error} error={"Could not save your plant"} />
            <FormImagePicker name="images" />
            <FormField maxLength={255} name="name" placeholder="Plant Name" />
            <FormPicker
              items={plantCategories}
              name="category"
              numberOfColumns={3}
              PickerItemComponent={CategoryPickerItem}
              placeholder="Category"
              width="50%"
            />
            <NectarButton title="Submit" onPress={handleSubmit as any} />
          </>
        )}
      </Formik>
    </SafeScreen>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});
export default PlantEditScreen;
