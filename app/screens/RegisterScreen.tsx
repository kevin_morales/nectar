import React, { Fragment, useState } from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";
import { Formik } from "formik";
import { ErrorMessage, FormField } from "../components/forms";
import SafeScreen from "../components/SafeScreen";
import NectarButton from "../components/NectarButton";
import NectarActivityIndicator from "../components/NectarActivityIndicator";
import usersApi from "../api/users";
import useAuth from "../auth/useAuth";
import authApi from "../api/auth";
import useApi from "../hooks/useApi";

interface RegisterProps {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

const initialValues: RegisterProps = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
};

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required().label("FirstName"),
  lastName: Yup.string().required().label("LastName"),
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
});

const RegisterScreen = () => {
  const registerApi = useApi(usersApi.register);
  const loginApi = useApi(authApi.login);
  const auth = useAuth();
  const [error, setError] = useState<any>();

  const handleSubmit = async (formValues: RegisterProps) => {
    const result = await registerApi.request(formValues);

    if (result.ok === false) {
      if (result.data) {
        setError(result.data.error);
      } else {
        setError("An unexpected error occurred");
      }
      return;
    }
    const { data: authToken } = await loginApi.request(
      formValues.email,
      formValues.password
    );
    auth.login(authToken.token);
  };

  return (
    <>
      <NectarActivityIndicator
        visible={registerApi.loading || loginApi.loading}
      />
      <SafeScreen style={styles.container}>
        <Formik
          initialValues={initialValues}
          onSubmit={(values) => handleSubmit(values)}
          validationSchema={validationSchema}
        >
          {({ handleSubmit }) => (
            <Fragment>
              <ErrorMessage error={error} visible={error} />
              <FormField
                autoCorrect={false}
                icon="account"
                name="firstName"
                placeholder="First Name"
              />
              <FormField
                autoCorrect={false}
                icon="account"
                name="lastName"
                placeholder="Last Name"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password"
                placeholder="Password"
                secureTextEntry
                textContentType="password"
              />
              <NectarButton title="Register" onPress={handleSubmit as any} />
            </Fragment>
          )}
        </Formik>
      </SafeScreen>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default RegisterScreen;
