import React, { ChangeEvent } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TextInputProps,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../config/colors";
import nectarStyles from "../config/styles";

interface Props extends TextInputProps {
  icon?: any;
  width?: string;
}

const NectarTextInput = ({ icon, width = "100%", ...props }: Props) => {
  return (
    <View style={[styles.container, { width }]}>
      {icon !== undefined && (
        <MaterialCommunityIcons
          name={icon}
          size={20}
          color={colors.medium}
          style={styles.icon}
        />
      )}
      <TextInput
        style={nectarStyles.text}
        placeholderTextColor={colors.medium}
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.light,
    borderRadius: 25,
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
  },
  icon: {
    marginRight: 10,
  },
});

export default NectarTextInput;
