import React from "react";
import LottieView from "lottie-react-native";
import { StyleSheet, View } from "react-native";

interface Props {
  visible: boolean;
}

const NectarActivityIndicator = ({ visible = false }: Props) => {
  if (!visible) return null;
  return (
    <View style={styles.overlay}>
      <LottieView
        autoPlay
        loop
        source={require("../assets/animations/loader.json")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  overlay: {
    position: "absolute",
    backgroundColor: "white",
    height: "100%",
    opacity: 0.8,
    width: "100%",
    zIndex: 1,
  },
});

export default NectarActivityIndicator;
