import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Modal,
  Button,
  FlatList,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../config/colors";
import NectarText from "./NectarText";
import SafeScreen from "./SafeScreen";
import PickerItem from "./PickerItem";
import { PlantCategory } from "../config/types";

export type PickerType = PlantCategory;

interface Props {
  icon?: any;
  items: PickerType[]; //fix with types.ts
  numberOfColumns?: number;
  onSelectItem: (item: PlantCategory) => void;
  placeholder: string;
  PickerItemComponent: (props: any) => JSX.Element;
  selectedItem?: PickerType;
  width?: string;
}

const Picker = ({
  icon,
  items,
  numberOfColumns = 1,
  onSelectItem,
  PickerItemComponent = PickerItem,
  placeholder,
  selectedItem,
  width = "100%",
}: Props) => {
  const [modalVisible, setModalVisible] = useState(false);

  const handlePress = (item: PlantCategory) => {
    setModalVisible(false);
    onSelectItem(item);
  };
  return (
    <>
      <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
        <View style={[styles.container, { width }]}>
          {icon !== undefined && (
            <MaterialCommunityIcons
              name={icon}
              size={20}
              color={colors.medium}
              style={styles.icon}
            />
          )}
          {selectedItem ? (
            <NectarText style={styles.text}>{selectedItem.label}</NectarText>
          ) : (
            <NectarText style={styles.placeholder}>{placeholder}</NectarText>
          )}
          <MaterialCommunityIcons
            name="chevron-down"
            size={20}
            color={colors.medium}
            style={styles.icon}
          />
        </View>
      </TouchableWithoutFeedback>
      <Modal visible={modalVisible} animationType="slide">
        <SafeScreen>
          <Button title="Close" onPress={() => setModalVisible(false)} />
          <FlatList
            data={items}
            keyExtractor={(item) => item.value.toString()}
            numColumns={numberOfColumns}
            renderItem={({ item }) => (
              <PickerItemComponent
                item={item}
                onPress={() => handlePress(item)}
              />
            )}
            style={{}}
          />
        </SafeScreen>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.light,
    borderRadius: 25,
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
  },
  icon: {
    marginRight: 10,
  },
  text: {
    flex: 1,
  },
  placeholder: {
    color: colors.medium,
    flex: 1,
  },
});

export default Picker;
