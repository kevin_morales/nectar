import { useFormikContext } from "formik";

import ErrorMessage from "./ErrorMessage";
import NectarTextInput from "../NectarTextInput";
import { TextInputProps } from "react-native";

interface Props extends TextInputProps {
  icon?: string;
  name: string;
}

const FormField = ({ icon, name, ...props }: Props) => {
  const { errors, setFieldTouched, setFieldValue, touched, values } =
    useFormikContext<any>();

  return (
    <>
      <NectarTextInput
        icon={icon}
        onBlur={() => setFieldTouched(name)}
        onChangeText={(text) => setFieldValue(name, text)}
        value={values[name]}
        {...props}
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
};

export default FormField;
