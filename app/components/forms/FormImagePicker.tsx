import { useFormikContext } from "formik";
import ImageInputList from "../ImageInputList";
import ErrorMessage from "./ErrorMessage";

interface Props {
  name: string;
}

const FormImagePicker = ({ name }: Props) => {
  const { errors, setFieldValue, touched, values } = useFormikContext<any>();
  const imageUris = values[name] as string[];

  const handleAdd = (uri?: string) => setFieldValue(name, [...imageUris, uri!]);

  const handleRemove = (uri?: string) =>
    setFieldValue(
      name,
      imageUris.filter((imageUri: string) => imageUri !== uri)
    );

  return (
    <>
      <ImageInputList
        imageUris={imageUris}
        onAddImage={handleAdd}
        onRemoveImage={handleRemove}
      />
      <ErrorMessage visible={touched[name]} error={errors[name]} />
    </>
  );
};

export default FormImagePicker;
