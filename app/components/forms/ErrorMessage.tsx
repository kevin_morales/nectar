import React from "react";
import { StyleSheet } from "react-native";
import NectarText from "../NectarText";

interface Props {
  visible: any;
  error: any;
}

const ErrorMessage = ({ visible, error }: Props) => {
  if (!visible || !error) return null;
  return <NectarText style={styles.error}>{error}</NectarText>;
};

const styles = StyleSheet.create({
  error: {
    color: "red",
  },
});

export default ErrorMessage;
