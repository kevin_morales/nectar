import { useFormikContext } from "formik";
import Picker, { PickerType } from "../Picker";
import ErrorMessage from "./ErrorMessage";

interface Props {
  items: PickerType[];
  name: string;
  numberOfColumns?: number;
  PickerItemComponent: (props: any) => JSX.Element;
  placeholder: string;
  width?: string;
}

const FormPicker = ({
  items,
  name,
  numberOfColumns,
  PickerItemComponent,
  placeholder,
}: Props): JSX.Element => {
  const { errors, setFieldValue, touched, values } = useFormikContext<any>();
  //@ts-ignore
  const selected = values[name];
  return (
    <>
      <Picker
        items={items}
        placeholder={placeholder}
        onSelectItem={(item) => setFieldValue(name, item)}
        selectedItem={selected}
        PickerItemComponent={PickerItemComponent}
        numberOfColumns={numberOfColumns}
      />
      <ErrorMessage visible={touched[name]} error={errors[name]} />
    </>
  );
};

export default FormPicker;
