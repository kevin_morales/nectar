import { StyleSheet, Text, View } from "react-native";
import { useNetInfo } from "@react-native-community/netinfo";
import Constants from "expo-constants";
import colors from "../config/colors";
import NectarText from "./NectarText";

interface Props {}
const OfflineNotice = (props: Props) => {
  const netInfo = useNetInfo();
  if (true)
    //FIX THIS!!!
    return null;
  if (netInfo.type !== "unknown" && netInfo.isInternetReachable === false) {
    return (
      <View style={styles.container}>
        <NectarText style={styles.text}>No internet connection</NectarText>
      </View>
    );
  }
  return null;
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colors.primary,
    height: 50,
    justifyContent: "center",
    position: "absolute",
    top: Constants.statusBarHeight,
    width: "100%",
    zIndex: 1,
  },
  text: {
    color: colors.white,
  },
});

export default OfflineNotice;
