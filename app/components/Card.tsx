import React from "react";
import {
  StyleProp,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from "react-native";
import { Image } from "react-native-expo-image-cache";
import colors from "../config/colors";
import NectarText from "./NectarText";

interface Props {
  imageUrl: string;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  subTitle: string;
  title: string;
  thumbnailUrl: string;
  date: string;
}

const Card = ({ imageUrl, onPress, style, title, thumbnailUrl }: Props) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.card, style]}>
        <Image
          style={styles.image}
          tint="light"
          preview={{ uri: thumbnailUrl }}
          uri={imageUrl}
        />
        <View style={styles.container}>
          <NectarText style={styles.title}>{title}</NectarText>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: colors.white,
    marginBottom: 20,
    overflow: "hidden",
  },
  container: {
    paddingBottom: 10,
    paddingHorizontal: 10,
    paddingTop: 5,
    flexDirection: "row",
  },
  image: {
    width: 150,
    height: 150,
  },
  title: {
    fontSize: 15,
    marginBottom: 7,
    flex: 1,
    flexWrap: "wrap",
  },
});

export default Card;
