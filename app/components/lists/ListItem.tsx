import React, { ReactNode } from "react";
import {
  Image,
  ImageSourcePropType,
  StyleSheet,
  TouchableHighlight,
  View,
} from "react-native";
import {
  Swipeable,
  GestureHandlerRootView,
} from "react-native-gesture-handler";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import NectarText from "../NectarText";
import colors from "../../config/colors";

interface Props {
  title: string;
  description?: string;
  image?: ImageSourcePropType;
  IconComponent?: ReactNode;
  onPress?: () => void;
  renderRightActions?: () => any;
  renderLeftActions?: () => any;
}

const ListItem = ({
  title,
  description,
  image,
  onPress,
  renderRightActions,
  renderLeftActions,
  IconComponent,
}: Props) => {
  return (
    <GestureHandlerRootView>
      {/*Note: Swipeable does not work without GestureHandlerRootView wrapper */}
      <Swipeable
        renderRightActions={renderRightActions}
        renderLeftActions={renderLeftActions}
      >
        <TouchableHighlight underlayColor={colors.light} onPress={onPress}>
          <View style={styles.container}>
            <>
            {IconComponent}
            {image !== undefined && (
              <Image style={styles.image} source={image} />
            )}
            <View style={styles.detailsContainer}>
              <NectarText style={styles.title} numberOfLines={1}>
                {title}
              </NectarText>
              {description && (
                <NectarText style={styles.subTitle} numberOfLines={2}>
                  {description}
                </NectarText>
              )}
            </View>
            </>
          </View>
        </TouchableHighlight>
      </Swipeable>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    padding: 20,
    backgroundColor: colors.white,
  },
  detailsContainer: {
    flex: 1,
    marginLeft: 10,
    justifyContent: "center",
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  title: {
    fontWeight: "500",
  },
  subTitle: {
    color: colors.medium,
  },
});

export default ListItem;
