import { StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import NectarText from "./NectarText";
import { PickerItemData } from "../config/types";

interface Props {
  item: PickerItemData;
  onPress: () => void;
}

const PickerItem = ({ item, onPress }: Props) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <NectarText style={styles.text}>{item.label}</NectarText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  text: {
    padding: 20,
  },
});

export default PickerItem;
