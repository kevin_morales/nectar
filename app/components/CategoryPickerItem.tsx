import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { CategoryPickerItemData } from "../config/types";
import Icon from "./Icon";
import NectarText from "./NectarText";
interface Props {
  item: CategoryPickerItemData;
  onPress: () => void;
}

const CategoryPickerItem = ({ item, onPress }: Props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Icon
          backgroundColor={item.backgroundColor}
          name={item.icon}
          size={80}
        />
      </TouchableOpacity>
      <NectarText style={styles.label}>{item.label}</NectarText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 15,
    alignItems: "center",
    width: "33%",
  },
  label: {
    marginTop: 5,
    textAlign: "center",
  },
});

export default CategoryPickerItem;
