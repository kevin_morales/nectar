import { Text, TextProps } from "react-native";
import React from "react";
import nectarStyles from "../config/styles";

const NectarText = ({ children, style, ...props }: TextProps) => {
  return (
    <Text style={[nectarStyles.text, style]} {...props}>
      {children}
    </Text>
  );
};

export default NectarText;
