import { useState } from "react";
import 'react-native-gesture-handler';
import { NavigationContainer } from "@react-navigation/native";
import AppLoading from "expo-app-loading";
import { logger } from "react-native-logs";
import nectarTheme from "./app/navigation/util/navigationTheme";
import NectarNavigator from "./app/navigation/NectarNavigator";
import OfflineNotice from "./app/components/OfflineNotice";
import AuthNavigator from "./app/navigation/auth/AuthNavigator";
import AuthContext, { AuthContextProps } from "./app/auth/context";
import { User } from "./app/config/types";
import authStorage from "./app/auth/storage";
const log = logger.createLogger();

const App = () => {
  const [user, setUser] = useState<User>();
  const [isReady, setIsReady] = useState(false);

  const authContextValues: AuthContextProps = {
    user,
    setUser: (user?: User) => setUser(user),
  };

  const restoreUser = async () => {
    const restoredUser = await authStorage.getUser();
    if (restoredUser) setUser(restoredUser);
  };

  if (!isReady) {
    return (
      <AppLoading
        startAsync={restoreUser}
        onFinish={() => setIsReady(true)}
        onError={(error) => log.error(error)}
      />
    );
  }

  return (
    <AuthContext.Provider value={authContextValues}>
      <OfflineNotice />
      <NavigationContainer theme={nectarTheme}>
        {user ? <NectarNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;
